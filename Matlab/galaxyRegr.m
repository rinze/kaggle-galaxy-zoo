function [ theta ] = galaxyRegr( featMx, y )
% Extract col without info

m = size(y,1);

%featMx = featureNormalize(featMx); %Not necessary with normEquation

%cols --> features
%rows --> samples
featMx = [ones(m, 1) featMx]; %add ones to the first column for theta0
classes = size(y,2);
theta = ones(size(featMx,2),classes);


for i = 1:classes
    theta(:,i) = normalEqn(featMx, y(:,i));
    
end


end

