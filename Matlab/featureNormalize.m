function [X_norm, mu, sigma] = featureNormalize(X)
%FEATURENORMALIZE Normalizes the features in X 
%   FEATURENORMALIZE(X) returns a normalized version of X where
%   the mean value of each feature is 0 and the standard deviation
%   is 1. This is often a good preprocessing step to do when
%   working with learning algorithms.


X_norm = X;
mu = zeros(1, size(X, 2));
sigma = zeros(1, size(X, 2));

     

mu = mean(X);
sigma = std(X); 

%X_norm = X - repmat([0 mu(2:3)],size(X,1),1);
%X_norm = X_norm ./ repmat([1 sigma(2:3)],size(X,1),1); 

X_norm = X_norm - repmat(mu,size(X,1),1);
X_norm = X_norm ./ repmat(sigma,size(X,1),1);







% ============================================================

end
